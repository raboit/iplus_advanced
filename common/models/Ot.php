<?php


Yii::import('common.models._base.BaseOt');

class Ot extends BaseOt
{
        
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
        public static function representingColumn() {
		return 'id';
	}
        
        public function misOts($id) {
		$criteria = new CDbCriteria;
		$criteria->compare('cliente_id', $id);
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
                        'pagination'=>false,
		));
	}
        
        public function validarDueno($id){
            
            $user = Yii::app()->user->getId();
            $tipo_admin = User::model()->findByPk($user);
            if($id == $user OR $tipo_admin->tipo == 'Admin')
                return true;
            else
                return false;
                      
            
        }
}
