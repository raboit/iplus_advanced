<?php

$this->breadcrumbs = array(
	Yii::t('app', 'Manage'),
);


$this->menu = array(
		array('label'=>Yii::t('app', 'Create') . ' ' . $model_ot->label(), 'url'=>array('crear')),
	);
 
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').slideToggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('ot-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('app', 'Manage') . ' ' . GxHtml::encode($model_ot->label(2)); ?></h1>

<p>
<?php echo Yii::t('app', 'Text Option Search'); ?></p>

<?php 
$this->widget('bootstrap.widgets.TbMenu', array(
	'type'=>'pills',
	'items'=>array(
		//array('label'=>Yii::t('app', 'Create'), 'icon'=>'icon-plus', 'url'=>Yii::app()->controller->createUrl('create'), 'linkOptions'=>array()),
                //array('label'=>Yii::t('app', 'List'), 'icon'=>'icon-th-list', 'url'=>Yii::app()->controller->createUrl('admin'),'active'=>true, 'linkOptions'=>array()),
		array('label'=>Yii::t('app', 'Search'), 'icon'=>'icon-search', 'url'=>'#', 'linkOptions'=>array('class'=>'search-button')),
		//array('label'=>Yii::t('app', 'Export to PDF'), 'icon'=>'icon-download', 'url'=>Yii::app()->controller->createUrl('GeneratePdf'), 'linkOptions'=>array('target'=>'_blank'), 'visible'=>true),
		array('label'=>Yii::t('app', 'Export to Excel'), 'icon'=>'icon-download', 'url'=>Yii::app()->controller->createUrl('GenerateExcel'), 'linkOptions'=>array('target'=>'_blank'), 'visible'=>true),
	),
));
?>

<div class="search-form">
<?php $this->renderPartial('_search', array(
	'model' => $model_ot,
)); ?>
</div><!-- search-form -->

<?php 
$this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'ot-grid',
	'dataProvider' => $model,
        'filter'=>$filtersForm,
        'type'=>'striped bordered condensed',
        'template'=>"{summary}{items}{pager}",
	'columns' => array(
		'id',
		array(
				'name'=>'cliente_id',
				'value'=>'GxHtml::valueEx($data->cliente)',
				'filter'=>GxHtml::listDataEx($model_clientes),
				),
		'fechaRegistro',
		'caracteristicas',
		'problema',
		'solucion',
		/*
		'precio',
		'observaciones',
		'estadoOt',
		'fechaTermino',
		'fechaEntrega',
		'estadoCuenta',
		*/
                array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        'htmlOptions'=>array('style'=>'width: 50px'),
			'template'=>'{view} {update}',
			'buttons'=>array(
				'view' => array(
					'label'=>'Ver Ot',
					'url'=>'Yii::app()->createUrl("ot/ver", array("id"=>$data->id))',
				),
				'update' => array(
					'label'=>'Actualizar Ot',
					'url'=>'Yii::app()->createUrl("ot/actualizar", array("id"=>$data->id))',
				),
			),
				
                ),               
	),
)); 

?>