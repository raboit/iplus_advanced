<?php
$this->breadcrumbs = array(
	"Mis Clientes" => array('misClientes'),
	"Ver Cliente",
);

$this->titleHeader=GxHtml::encode(GxHtml::valueEx($model));
$this->subTitleHeader=Yii::t('app', 'View') .' ' . GxHtml::encode($model->label());

$this->menu=array(
	array('label'=>Yii::t('app', 'Crear Ot'), 'url'=>array('ot/crearCliente', 'cliente_id' => $model->id)),
	array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('actualizar', 'id' => $model->id)),
	array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('eliminar', 'id' => $model->id), 'confirm'=>'Seguro deseas eliminar este cliente?')),

);
?>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
'telefono',
'celular',
'email',
	),
)); ?>

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered',
    'dataProvider' => $model_ots,
    'enableSorting'=>false,
    'template'=> '{summary}{pager}{items}{pager}',
    'columns' => array(
                'fechaRegistro',
                'caracteristicas',
        	'problema',
                'solucion',
                'precio',
                'estadoOt',
                /*'fechaTermino',
                'fechaEntrega',*/
                'estadoCuenta',
                array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        'htmlOptions'=>array('style'=>'width: 50px'),
			'template'=>'{view} {update}',
			'buttons'=>array(
				'view' => array(
					'label'=>'Ver Ot',
					'url'=>'Yii::app()->createUrl("ot/ver", array("id"=>$data->id))',
				),
				'update' => array(
					'label'=>'Actualizar Ot',
					'url'=>'Yii::app()->createUrl("ot/actualizar", array("id"=>$data->id))',
				),
			),
				
                ),
    ),
));?>