<div class="form">

    
<?php $this->widget('ext.EChosen.EChosen' ); ?>
<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'user-change-password',
	'enableAjaxValidation' => true,
));
?>
<?php $this->widget('bootstrap.widgets.TbAlert', array(
        'block'=>true, // display a larger alert block?
        'fade'=>true, // use transitions?
        'closeText'=>'&times;', // close link text - if set to false, no close link is displayed
        'alerts'=>array( // configurations per alert type
            'success'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), // success, info, warning, error or danger
        ),
    ));
?>
	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'password_anterior'); ?>
		<?php echo $form->passwordField($model, 'password_anterior', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'password_anterior'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'password_nuevo'); ?>
		<?php echo $form->passwordField($model, 'password_nuevo', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'password_nuevo'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'password_nuevo_repetir'); ?>
		<?php echo $form->passwordField($model, 'password_nuevo_repetir', array('maxlength' => 45)); ?>
		<?php echo $form->error($model,'password_nuevo_repetir'); ?>
		</div><!-- row -->
                
<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'), array('onClick' => "this.disabled=true;this.value='".Yii::t('app', 'Enviando')."';this.form.submit();"));
$this->endWidget();
?>
</div><!-- form -->