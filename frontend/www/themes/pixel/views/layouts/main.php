<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo Yii::app()->charset; ?>" />
<meta name="language" content="<?php echo Yii::app()->language; ?>" />
<title><?php echo CHtml::encode(Yii::app()->name); ?></title>
<link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/favicon.ico" type="image/x-icon" />
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style.default.css" type="text/css" />
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/styles.css">
<?php Yii::app()->bootstrap->register(); ?>

<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery-migrate-1.1.1.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.uniform.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/flot/jquery.flot.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/flot/jquery.flot.resize.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/custom.js"></script>
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->
</head>

<body>

<div class="mainwrapper">
    
    <div class="header">
        <div class="logo">
            <a href="#">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/logo_iplus.png" alt="" />
            </a>
        </div>
        <div class="headerinner">
            <ul class="headmenu">               
                <li class="odd">
                    <a class="dropdown-toggle" data-toggle="dropdown" data-target="#">                    
                        <span class="head-icon head-users"></span>
                        <span class="headmenu-label">Clientes</span>
                    </a>
                    <ul class="dropdown-menu newusers">
                        <li class="nav-header">Clientes</li>
                        <li><a href="<?php echo Yii::app()->createUrl('cliente/crear');?>"><span class="icon-user"></span> Crear nuevo</a></li>
                        <li><a href="<?php echo Yii::app()->createUrl('cliente/misClientes');?>"><span class="icon-list-alt"></span> Mis clientes</a></li>
                    </ul>
                </li>
                <li class="odd">
                    <a class="dropdown-toggle" data-toggle="dropdown" data-target="#">
                    
                    <span class="head-icon head-bar"></span>
                    <span class="headmenu-label">Órdenes de trabajo</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="nav-header">Órdenes de trabajo</li>
                        <li><a href="<?php echo Yii::app()->createUrl('ot/crear');?>"><span class="icon-signal"></span> Crear nuevo</a></li>
                        <li><a href="<?php echo Yii::app()->createUrl('ot/misOts');?>"><span class="icon-list-alt"></span> Ver todos</a></li>
                    </ul>
                </li>
                <li class="right">
                    <div class="userloggedinfo">
                        <div class="userinfo">
                            <?php 
                            if(Yii::app()->user->isGuest):
                            ?>
                            <a href="<?php echo Yii::app()->createUrl('site/login'); ?>" class="btn btn-success btn-large">Entrar »</a>
                             <a href="<?php echo Yii::app()->createUrl('user/crear'); ?>" class="btn btn-warning btn-large">Registrar »</a>
                                <?php
                                else:
                                 ?>   
                               
                            <h5>
                                <?php echo Yii::app()->user->name; ?><small>- <?php echo Yii::app()->user->getState('email');?></small></h5>
                            <ul>                                
                                <li><a href="<?php echo Yii::app()->createUrl('/user/changePassword/', array("id"=>Yii::app()->user->id)); ?>">Editar contraseña</a></li>
                                <li><a href="<?php echo Yii::app()->createUrl('site/logout'); ?>">Salir</a></li>
                            </ul>
                            <?php
                                endif; 
                                ?>
                        </div>
                    </div>
                </li>
            </ul><!--headmenu-->
        </div>
    </div>
    
    <div class="leftpanel">
        
        <div class="leftmenu">
            <?php
                        $this->widget('bootstrap.widgets.TbNav', array(
                            'type'=> TbHtml::NAV_TYPE_TABS,
                            'items'=> array_merge(
                                $this->menu,
                                array(
                                    array('label'=>Yii::t('app', 'Menu Secundario')),
                                    array('label'=>Yii::t('app', 'Ayuda'), 'url'=>array('site/ayuda'), 'icon'=>'list white'),
                                    array('label'=>Yii::t('app', 'Acerca de iPlus') , 'url'=>array('site/precios'), 'icon'=>'file white'),
                            )),
                            'stacked'=>true,
                            //'htmlOptions'=>array('class'=>'nav-stacked'),
                        ));
                        
            ?>
               
                
           
        </div><!--leftmenu-->
        
    </div><!-- leftpanel -->
    
    <div class="rightpanel">
        
        <?php $this->widget('bootstrap.widgets.TbBreadcrumb', array(
                'links'=>$this->breadcrumbs,
                'htmlOptions'=>array('class'=>'breadcrumbs'),
        )); ?>
        
        <div class="maincontent">
            <div class="maincontentinner">
                
                <?php echo $content; ?>
            
            <div class="clearfix"><br /></div>
            
          
          <div class="footer">
                    <div class="footer-left">
                        <?php echo Yii::app()->params['empresa'] ?> &REG;  <?php echo date('Y'); ?> Todos los derechos reservados.<br/>
		
                    </div>
                    <div class="footer-right">
                        <span>Copyright &copy;  Desarrollado por <a href="http://www.raboit.com/" rel="external">Rabo IT</a></span>
                    </div>
                </div><!--footer-->
          
            </div><!--maincontentinner-->
        </div><!--maincontent-->
        </div><!--rightpanel-->
    
</div><!--mainwrapper-->

</body>
</html>

