<?php

class ClienteController extends GxController {

        public function filters() {
                //return array('rights');
        }
	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Cliente'),
		));
	}
        
        
        public function actionVer($id) {
            
            $model = $this->loadModel($id, 'Cliente');
            if($model->validarDueno($model->user_id)){
                $model_ots= new Ot;
                $model_ots=$model_ots->misOts($id);
                    $this->render('ver', array(
                            'model' => $model,
                            'model_ots' => $model_ots,
                    ));
            }
            else
               throw new CHttpException(400, Yii::t('app', 'You are not authorized to perform this action.'));
        
        
        }
        
        public function actionMisClientes() {

            $model = $this->loadModel(Yii::app()->user->id, 'User');
            $model_clientes= new Cliente;
            $model_clientes=$model_clientes->misClientes($model->id);
		$this->render('misClientes', array(
			'model' => $model,
                        'model_clientes' => $model_clientes,
                        
                       

		));
	}

	public function actionCreate() {
		$model = new Cliente;

		$this->performAjaxValidation($model, 'cliente-form');

		if (isset($_POST['Cliente'])) {
			$model->setAttributes($_POST['Cliente']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}
        
        public function actionCrear() {
		$model = new Cliente;
                $model->user_id = Yii::app()->user->id;

		$this->performAjaxValidation($model, 'cliente-form');

		if (isset($_POST['Cliente'])) {
			$model->setAttributes($_POST['Cliente']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('misClientes'));
			}
		}

		$this->render('crear', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Cliente');

		$this->performAjaxValidation($model, 'cliente-form');

		if (isset($_POST['Cliente'])) {
			$model->setAttributes($_POST['Cliente']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}
        
        public function actionActualizar($id) {
		$model = $this->loadModel($id, 'Cliente');
                if($model->validarDueno($model->user_id)){
		$this->performAjaxValidation($model, 'cliente-form');

		if (isset($_POST['Cliente'])) {
			$model->setAttributes($_POST['Cliente']);

			if ($model->save()) {
				$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('actualizar', array(
				'model' => $model,
				));
	}
        else
                throw new CHttpException(400, Yii::t('app', 'You are not authorized to perform this action.'));
        
        
        }

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Cliente')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}
        
        public function actionEliminar($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Cliente')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('misClientes'));
		} else
			throw new CHttpException(400, Yii::t('app', 'You are not authorized to perform this action.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Cliente');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}
        

	public function actionAdmin() {
                $session = new CHttpSession;
                $session->open();
		$model = new Cliente('search');
		$model->unsetAttributes();

		if (isset($_GET['Cliente'])){
			$model->setAttributes($_GET['Cliente']);
                }

                $session['Cliente_model_search'] = $model;
                
		$this->render('admin', array(
			'model' => $model,
		));
	}
        
        public function actionVerTodos() {
                $session = new CHttpSession;
                $session->open();
		$model = new Cliente('search');
		$model->unsetAttributes();

		if (isset($_GET['Cliente'])){
			$model->setAttributes($_GET['Cliente']);
                }

                $session['Cliente_model_search'] = $model;
                
		$this->render('verTodos', array(
			'model' => $model,
		));
	}
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
        public function actionGenerateExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Cliente_model_search']))
               {
                $model = $session['Cliente_model_search'];
                $model = Cliente::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Cliente::model()->findAll();
             
             $this->toExcel($model, array('id', 'nombre', 'telefono', 'celular', 'email'), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGeneratePdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Cliente_model_search']))
               {
                $model = $session['Cliente_model_search'];
                $model = Cliente::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Cliente::model()->findAll();
             
             $this->toExcel($model, array('id', 'nombre', 'telefono', 'celular', 'email'), date('Y-m-d-H-i-s'), array(), 'PDF');
	}
}