<?php
/**
 *
 * main.php layout
 *
 * @author Antonio Ramirez <amigo.cobos@gmail.com>
 * @link http://www.ramirezcobos.com/
 * @link http://www.2amigos.us/
 * @copyright 2013 2amigOS! Consultation Group LLC
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */
?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo Yii::app()->language; ?>" lang="<?php echo Yii::app()->language; ?>"> <!--<![endif]-->
<head>
        <meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
            
	<meta http-equiv="Content-Type" content="text/html; charset=<?php echo Yii::app()->charset; ?>" />
	<meta name="language" content="<?php echo Yii::app()->language; ?>" />
        <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/favicon.ico" type="image/x-icon" /> 
        <?php Yii::app()->bootstrap->register(); ?>
        <title><?php echo CHtml::encode(Yii::app()->name); ?> TEST</title>
        	
        
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/libs/modernizr-2.6.2-respond-1.1.0.min.js"></script>
	
        <?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/css/styles.css'); ?>
	
	
</head>
<body>
<?php $this->widget('bootstrap.widgets.TbNavbar',array(
    'collapse'=>true,
    'items'=>array(
        array(
            'class'=>'bootstrap.widgets.TbNav',
            'items'=>array(
                array('label'=>'Inicio', 'url'=>array('/site/index')),
                array('label'=>'Usuarios', 'url'=>'#', 'visible'=>!Yii::app()->user->isGuest, 'items'=>array(
                    array('label'=>'Usuarios', 'url'=>array('/user/index')),
                    array('label'=>'Roles', 'url'=>array('/rights/')),
                    TbHtml::menuDivider(),
                    array('label'=>'Cambiar Contraseña', 'url'=>array('/user/changePassword/', "id"=>Yii::app()->user->id)),
                )),
                array('label'=>'Ingresar', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
                array('label'=>'Salir ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
            ),
        ),
    ),
)); ?>


<div class="container" id="page">
        
        <?php $this->widget('bootstrap.widgets.TbBreadcrumb', array(
                'links'=>$this->breadcrumbs,
        )); ?>

	<?php echo $content; ?>

	<div class="clear"></div>
        
        <hr>

	<div id="footer">
                Copyright &copy; <?php echo date('Y'); ?> por <?php echo Yii::app()->params['empresa'] ?><br/>
		Todos los derechos reservados.<br/>
		Desarrollado por <a href="http://www.raboit.com/" rel="external">Rabo IT</a>.
	</div><!-- footer -->

</div><!-- page -->




<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/plugins.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/main.js"></script><script>
	var _gaq = [
		['_setAccount', 'UA-40741810-4'],
		['_trackPageview']
	];
	(function (d, t) {
		var g = d.createElement(t), s = d.getElementsByTagName(t)[0];
		g.src = ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js';
		s.parentNode.insertBefore(g, s)
	}(document, 'script'));
</script>
</body>
</html>