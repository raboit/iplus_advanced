<?php

class PlanificacionSemestralController extends GxController {

        public function filters() {
                return array('rights');
        }

	public function actionIndex() {
                $this->redirect(array('listar'));
	}

	public function actionListar() {
		$dataProvider = new CActiveDataProvider('PlanificacionSemestral');
		$this->render('listar', array(
			'dataProvider' => $dataProvider,
		));
	}        
        
	public function actionVer($id) {
		$this->render('ver', array(
			'model' => $this->loadModel($id, 'PlanificacionSemestral'),
		));
	}

	public function actionCrear() {
		$model = new PlanificacionSemestral;

		$this->performAjaxValidation($model, 'planificacion-semestral-form');

		if (isset($_POST['PlanificacionSemestral'])) {
			$model->setAttributes($_POST['PlanificacionSemestral']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('crear', array( 'model' => $model));
	}

	public function actionActualizar($id) {
		$model = $this->loadModel($id, 'PlanificacionSemestral');

		$this->performAjaxValidation($model, 'planificacion-semestral-form');

		if (isset($_POST['PlanificacionSemestral'])) {
			$model->setAttributes($_POST['PlanificacionSemestral']);

			if ($model->save()) {
				$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('actualizar', array(
				'model' => $model,
				));
	}

	public function actionBorrar($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'PlanificacionSemestral')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('administrar'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionAdministrar() {
                $session = new CHttpSession;
                $session->open();
		$model = new PlanificacionSemestral('search');
		$model->unsetAttributes();

		if (isset($_GET['PlanificacionSemestral'])){
			$model->setAttributes($_GET['PlanificacionSemestral']);
                }

                $session['PlanificacionSemestral_model_search'] = $model;
                
		$this->render('administrar', array(
			'model' => $model,
		));
	}
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
             
        
        public function actionGenerarExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['PlanificacionSemestral_model_search']))
               {
                $model = $session['PlanificacionSemestral_model_search'];
                $model = PlanificacionSemestral::model()->findAll($model->search()->criteria);
               }
               else
                 $model = PlanificacionSemestral::model()->findAll();
             $this->toExcel($model, array('id', 'calendarioDocente', 'fecha_creacion', 'fecha_proposicion', 'fecha_respuesta', 'estado', 'fecha_inicio', 'fecha_termino'), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGenerarPdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['PlanificacionSemestral_model_search']))
               {
                $model = $session['PlanificacionSemestral_model_search'];
                $model = PlanificacionSemestral::model()->findAll($model->search()->criteria);
               }
               else
                 $model = PlanificacionSemestral::model()->findAll();
             $this->toExcel($model, array('id', 'calendarioDocente', 'fecha_creacion', 'fecha_proposicion', 'fecha_respuesta', 'estado', 'fecha_inicio', 'fecha_termino'), date('Y-m-d-H-i-s'), array(), 'PDF');
	}

//-------------------------------------------------------------------------------------------------------------        
        
	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'PlanificacionSemestral'),
		));
	}

	public function actionCreate() {
		$model = new PlanificacionSemestral;

		$this->performAjaxValidation($model, 'planificacion-semestral-form');

		if (isset($_POST['PlanificacionSemestral'])) {
			$model->setAttributes($_POST['PlanificacionSemestral']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'PlanificacionSemestral');

		$this->performAjaxValidation($model, 'planificacion-semestral-form');

		if (isset($_POST['PlanificacionSemestral'])) {
			$model->setAttributes($_POST['PlanificacionSemestral']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'PlanificacionSemestral')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

        //Index
	public function actionList() {
		$dataProvider = new CActiveDataProvider('PlanificacionSemestral');
                //$this->render('index', array(
		$this->render('list', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
                $session = new CHttpSession;
                $session->open();
		$model = new PlanificacionSemestral('search');
		$model->unsetAttributes();

		if (isset($_GET['PlanificacionSemestral'])){
			$model->setAttributes($_GET['PlanificacionSemestral']);
                }

                $session['PlanificacionSemestral_model_search'] = $model;
                
		$this->render('admin', array(
			'model' => $model,
		));
	}
}