<?php

$this->breadcrumbs = array(
	'OT' => array('listar'),
	Yii::t('app', 'Mis ots'),
);

$this->menu = array(
        array('label'=>Yii::t('app', 'Operations')),
        array('label'=>Yii::t('app', 'Create') . ' OT ', 'url'=>array('crear'), 'icon'=>'file white'),
        array('label'=>Yii::t('app', 'Export')),
        array('label'=>Yii::t('app', 'Export to Excel'), 'url'=>Yii::app()->controller->createUrl('GenerarExcel'), 'linkOptions'=>array('target'=>'_blank'), 'icon'=>'download-alt white'),
        array('label'=>Yii::t('app', 'Other|Others', 2)),
        array('label'=>Yii::t('app', 'Back'), 'url'=>'javascript:history.back()', 'icon'=>'arrow-left white'),
);
?>
<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').slideToggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('ot-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="title-menu">
        <?php echo TbHtml::pageHeader(Yii::t('app', 'Mis Ots'), null); ?>
</div>


<?php 
$this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'ot-grid',
	'dataProvider' => $model,
        'filter'=>$filtersForm,
        'type'=>'striped bordered condensed',
        'template'=>"{summary}{items}{pager}",
	'columns' => array(
		'id',
		array(
				'name'=>'cliente_id',
				'value'=>'GxHtml::valueEx($data->cliente)',
				'filter'=>GxHtml::listDataEx($model_clientes),
				),
		'fechaRegistro',
		'caracteristicas',
		'problema',
		'solucion',
		/*
		'precio',
		'observaciones',
		'estadoOt',
		'fechaTermino',
		'fechaEntrega',
		'estadoCuenta',
		*/
                array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        'htmlOptions'=>array('style'=>'width: 50px'),
			'template'=>'{view} {update}',
			'buttons'=>array(
				'view' => array(
					'label'=>'Ver Ot',
					'url'=>'Yii::app()->createUrl("ot/ver", array("id"=>$data->id))',
				),
				'update' => array(
					'label'=>'Actualizar Ot',
					'url'=>'Yii::app()->createUrl("ot/actualizar", array("id"=>$data->id))',
				),
			),
				
                ),               
	),
)); 

?>