<?php
/**
 *
 * main.php configuration file
 *
 * @author Antonio Ramirez <amigo.cobos@gmail.com>
 * @link http://www.ramirezcobos.com/
 * @link http://www.2amigos.us/
 * @copyright 2013 2amigOS! Consultation Group LLC
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */
return array(
	'preload' => array('log'),
        //'name'=>'BASE YII',
	'aliases' => array(
		'frontend' => dirname(__FILE__) . '/../..' . '/frontend',
		'common' => dirname(__FILE__) . '/../..' . '/common',
		'backend' => dirname(__FILE__) . '/../..' . '/backend',
		'vendor' => dirname(__FILE__) . '/../..' . '/common/lib/vendor',
	),
    /** 
     * Configuración local
     * Tiempo
     * Lenguaje
     * cotejamiento
     */
        'language'=>'es',
        'sourceLanguage'=>'en',
        'charset'=>'utf-8',
        'timeZone' => 'America/Santiago',
    
	'import' => array(
		'common.extensions.components.*',
                'common.extensions.*',
		'common.components.*',
		'common.helpers.*',
		'common.models.*',
                'common.models._base.*',
            
                'common.extensions.giix.components.*',
                'common.helpers.TbHtml',
            
		'application.controllers.*',
		'application.extensions.*',
		'application.helpers.*',
		'application.models.*'
	),
	'components' => array(
		'db' => array(
			'connectionString' => 'mysql:host=localhost;dbname=iplus_db',
			'username' => 'root',
			'password' => '',
			'enableProfiling' => true,
			'enableParamLogging' => true,
			'charset' => 'utf8',
		),
                
                
		'errorHandler' => array(
			'errorAction' => 'site/error',
		),
                'format'=>array(
                        'class'=>'common.components.Formatter',
                ),
		'log' => array(
			'class'  => 'CLogRouter',
			'routes' => array(
				array(
					'class'        => 'CDbLogRoute',
					'connectionID' => 'db',
					'levels'       => 'error, warning',
				),
			),
		),
	),
	'params' => array(
		// php configuration
		'php.defaultCharset' => 'utf-8',
		'php.timezone'       => 'UTC',
                'adminEmail'=>'contacto@raboit.com',
                
                //Aplicación
                'nombreAplicacion' => 'Base YII',
                'empresa' => 'Rabo IT',
                
                //Formatos para fecha, solo modificar si sabe lo que hace
		'dateOutcomeFormat' => 'Y-m-d',
		'dateTimeOutcomeFormat' => 'Y-m-d H:i:s',
		'dateIncomeFormat' => 'yyyy-MM-dd',
		'dateTimeIncomeFormat' => 'yyyy-MM-dd hh:mm:ss',
                
                'dateFormat' => 'dd/mm/yyyy',
	)
);