<?php

$this->breadcrumbs = array(
	$model->label(2) => array('listar'),
	Yii::t('app', 'Manage'),
);

$this->menu = array(
        array('label'=>Yii::t('app', 'Operations')),
        array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('listar'), 'icon'=>'list'),
        array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('crear'), 'icon'=>'file'),
        array('label'=>Yii::t('app', 'Export')),
        array('label'=>Yii::t('app', 'Export to Excel'), 'url'=>Yii::app()->controller->createUrl('GenerarExcel'), 'linkOptions'=>array('target'=>'_blank'), 'icon'=>'download-alt'),
        array('label'=>Yii::t('app', 'Other|Others', 2)),
        array('label'=>Yii::t('app', 'Back'), 'url'=>'javascript:history.back()', 'icon'=>'arrow-left'),
);
?>

<div class="title-menu">
        <?php echo TbHtml::pageHeader(Yii::t('app', 'Manage') . ' ' . GxHtml::encode($model->label(2)), null); ?>
</div>

<p>
<?php echo Yii::t('app', 'Text Option Search'); ?></p>

<div class="buttons-admin">
<?php 
       echo CHtml::link(Yii::t('app', 'Advanced Search'),'#',array('class'=>'search-button btn'));
?>
</div>
<div class="search-form">
<?php $this->renderPartial('_busqueda', array(
	'model' => $model,
)); ?>
</div><!-- search-form -->

<?php 
$this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'ot-grid',
	'dataProvider' => $model,
        'filter'=>$filtersForm,
        'type'=>'striped bordered condensed',
        'template'=>"{summary}{items}{pager}",
	'columns' => array(
		'id',
		array(
				'name'=>'cliente_id',
				'value'=>'GxHtml::valueEx($data->cliente)',
				'filter'=>GxHtml::listDataEx($model_clientes),
				),
		'fechaRegistro',
		'caracteristicas',
		'problema',
		'solucion',
		/*
		'precio',
		'observaciones',
		'estadoOt',
		'fechaTermino',
		'fechaEntrega',
		'estadoCuenta',
		*/
                array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        'htmlOptions'=>array('style'=>'width: 50px'),
			'template'=>'{view} {update}',
			'buttons'=>array(
				'view' => array(
					'label'=>'Ver Ot',
					'url'=>'Yii::app()->createUrl("ot/ver", array("id"=>$data->id))',
				),
				'update' => array(
					'label'=>'Actualizar Ot',
					'url'=>'Yii::app()->createUrl("ot/actualizar", array("id"=>$data->id))',
				),
			),
				
                ),               
	),
)); 

?>