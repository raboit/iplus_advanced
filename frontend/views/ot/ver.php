<?php

$this->breadcrumbs = array(
	$model->label(2) => array('listar'),
	GxHtml::valueEx($model),
);

$this->menu=array(
        array('label'=>Yii::t('app', 'Operations')),
        array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('listar'), 'icon'=>'list white'),
        array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('crear'), 'icon'=>'file white'),
        array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('actualizar', 'id' => $model->id), 'icon'=>'pencil white'),
        array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('borrar', 'id' => $model->id), 'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?')), 'icon'=>'trash white'),
        array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('administrar'), 'icon'=>'list-alt white'),
        array('label'=>Yii::t('app', 'Other|Others', 2)),
        array('label'=>Yii::t('app', 'Back'), 'url'=>'javascript:history.back()', 'icon'=>'arrow-left white'),
);
?>

<?php echo TbHtml::pageHeader(Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)), null); ?>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
'id',
array(
			'name' => 'cliente',
			'type' => 'raw',
			'value' => $model->cliente !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->cliente)), array('cliente/ver', 'id' => GxActiveRecord::extractPkValue($model->cliente, true))) : null,
			),
'fechaRegistro',
'caracteristicas',
'problema',
'solucion',
'precio',
'observaciones',
'estadoOt',
'fechaTermino',
'fechaEntrega',
'estadoCuenta',
	),
)); ?>


<?php echo TbHtml::buttonGroup(array(
        array(
            'label'=>'Finalizar',
            'url'=>array('ot/finalizarOt','id'=>$model->id),
            'icon'=>'ok',
            'color' => TbHtml::BUTTON_COLOR_PRIMARY,
            ),
         array(
            'label'=>'Entregar',            
            'url'=>array('ot/entregarOt','id'=>$model->id),
            'icon'=>'share',
            'color' => TbHtml::BUTTON_COLOR_INFO,
            ),
        array(
            'label'=>'Pagar',
            'url'=>array('ot/pagarOt','id'=>$model->id),
            'icon'=>'thumbs-up',
            'color' => TbHtml::BUTTON_COLOR_SUCCESS,
            ),
       

    )); ?>

