<div class="form">

    
<?php $this->widget('ext.EChosen.EChosen', array('target' => 'select')); ?>
<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'user-form',
	'enableAjaxValidation' => false ,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model, 'username', array('maxlength' => 128)); ?>
		<?php echo $form->error($model,'username'); ?>
		</div><!-- row -->
                <div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model, 'password', array('maxlength' => 128)); ?>
		<?php echo $form->error($model,'password'); ?>
		</div><!-- row -->
                
		<div class="row">
		<?php echo $form->labelEx($model,'password2', array('label'=>'Re-ingrese la contraseña')); ?>
		<?php echo $form->passwordField($model, 'password2', array('maxlength' => 128)); ?>
		<?php echo $form->error($model,'password2'); ?>
		</div><!-- row -->

               
                
		<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model, 'email', array('maxlength' => 128)); ?>
		<?php echo $form->error($model,'email'); ?>
		</div><!-- row -->
                
                <?php echo $form->labelEx($model, 'validacion'); ?>
                <?php $this->widget('application.extensions.recaptcha.EReCaptcha', 
                   array('model'=>$model, 'attribute'=>'validacion',
                         'theme'=>'red', 'language'=>'es_ES', 
                         'publicKey'=>'6LdjgdwSAAAAAJCjMjKfxg79vzYcP2_ho_vogFwc')) ?>
                <?php echo $form->labelEx($model, 'validacion'); ?>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'), array('onClick' => "this.disabled=true;this.value='".Yii::t('app', 'Enviando')."';this.form.submit();"));
$this->endWidget();
?>
</div><!-- form -->