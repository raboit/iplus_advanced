<?php

$this->breadcrumbs = array(
	"Mis Clientes" => array('misClientes'),
	Yii::t('app', 'Create'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Back'), 'url'=>Yii::app()->request->urlReferrer),
);
?>

<h1><?php echo Yii::t('app', 'Create') . ' ' . GxHtml::encode($model->label()); ?></h1>

<?php
$this->renderPartial('_crear', array(
		'model' => $model,
		'buttons' => 'create'));
?>