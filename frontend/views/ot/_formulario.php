
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id' => 'ot-form',
        'layout'=>TbHtml::FORM_LAYOUT_HORIZONTAL,
        'enableAjaxValidation' => true,
));
?>

	<p class="help-block">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<?php echo $form->dropDownListControlGroup($model,'cliente_id', GxHtml::listDataEx($model_clientes), array('class'=>'span3 selectpicker', 'empty'=>'', 'placeholder'=> Yii::t('app', 'Select') ));?>
		<?php echo $form->textFieldControlGroup($model,'fechaRegistro', array('span'=>3)); ?>
		<?php echo $form->textFieldControlGroup($model,'caracteristicas', array('span'=>3, 'maxlength'=>200)); ?>
		<?php echo $form->textFieldControlGroup($model,'problema', array('span'=>3, 'maxlength'=>200)); ?>
		<?php echo $form->textFieldControlGroup($model,'solucion', array('span'=>3, 'maxlength'=>200)); ?>
		<?php echo $form->textFieldControlGroup($model,'precio', array('span'=>3, 'maxlength'=>11)); ?>
		<?php echo $form->textFieldControlGroup($model,'observaciones', array('span'=>3, 'maxlength'=>200)); ?>
		<?php echo $form->textFieldControlGroup($model,'estadoOt', array('span'=>3, 'maxlength'=>45)); ?>
		<?php echo $form->textFieldControlGroup($model,'fechaTermino', array('span'=>3)); ?>
		<?php echo $form->textFieldControlGroup($model,'fechaEntrega', array('span'=>3)); ?>
		<?php echo $form->textFieldControlGroup($model,'estadoCuenta', array('span'=>3, 'maxlength'=>45)); ?>

<?php echo TbHtml::formActions(array(
    TbHtml::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), array('color' => TbHtml::BUTTON_COLOR_PRIMARY, 'id'=>'buttonStateful', 'data-loading-text'=>  Yii::t('app', 'Loading...'))),
    TbHtml::resetButton(Yii::t('app', 'Reset')),
));
?>
<?php $this->endWidget(); ?>        
<?php $this->widget('ext.select2.ESelect2'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/buttonReset.js', CClientScript::POS_END); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/buttonStateful.js', CClientScript::POS_END); ?>
