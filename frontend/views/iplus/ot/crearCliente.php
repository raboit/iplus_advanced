<?php

$this->breadcrumbs = array(
	"Mis Cliente" => array('cliente/misClientes'),
        "Ver Cliente" => array('cliente/ver', 'id' => $model->cliente_id),
	Yii::t('app', 'Create'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Back'), 'url'=>Yii::app()->request->urlReferrer),
);
?>

<h1><?php echo Yii::t('app', 'Create') . ' ' . GxHtml::encode($model->label()); ?></h1>

<?php
$this->renderPartial('_crearCliente', array(
		'model' => $model,
                'model_cliente' => $model_cliente,
		'buttons' => 'create'));
?>