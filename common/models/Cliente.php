<?php


Yii::import('common.models._base.BaseCliente');

class Cliente extends BaseCliente
{
        
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        public function rules() {
                return array_merge(parent::rules(), array(
                    array('email','email'),
                ));
            
            }
            
        
        public function misClientes($id) {
		$criteria = new CDbCriteria;
		$criteria->compare('user_id', $id);
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
                        'pagination'=>false,
		));
	}
        
        public function validarDueno($id){
            
            $user = Yii::app()->user->getId();
            $tipo_admin = User::model()->findByPk($user);
            if($id == $user OR $tipo_admin->tipo == 'Admin')
                return true;
            else
                return false;
                      
            
        }
}
