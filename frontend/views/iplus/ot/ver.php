<?php

$this->breadcrumbs = array(
	"Mis Ots" => array('misOts'),
	"Ver Ot",
);

$this->menu=array(
	array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('actualizar', 'id' => $model->id)),
	array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('eliminar', 'id' => $model->id), 'confirm'=>'Seguro desea eliminar esta Ot?')),
	//array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('crear')),
        array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('misOts')),
	);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ': ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
'id',
array(
			'name' => 'cliente',
			'type' => 'raw',
			'value' => $model->cliente !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->cliente)), array('cliente/ver', 'id' => GxActiveRecord::extractPkValue($model->cliente, true))) : null,
			),
'fechaRegistro',
'caracteristicas',
'problema',
'solucion',
'precio',
'observaciones',
'estadoOt',
'fechaTermino',
'fechaEntrega',
'estadoCuenta',
	),
)); ?>


<?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
    'buttons'=>array(
        array(
            'label'=>'Finalizar',
            'url'=>array('ot/finalizarOt','id'=>$model->id),
            'icon'=>'ok white',
            'type'=>'primary'
            ),
         array(
            'label'=>'Entregar',            
            'url'=>array('ot/entregarOt','id'=>$model->id),
            'icon'=>'icon-share white',
            'type'=>'info',
            ),
        array(
            'label'=>'Pagar',
            'url'=>array('ot/pagarOt','id'=>$model->id),
            'icon'=>'icon-thumbs-up white',
            'type'=>'success',
            ),
       

    ),
)); ?>
