<?php

class AlumnoController extends GxController {

        public function filters() {
                return array('rights');
        }

	public function actionIndex() {
                $this->redirect(array('listar'));
	}

	public function actionListar() {
		$dataProvider = new CActiveDataProvider('Alumno');
		$this->render('listar', array(
			'dataProvider' => $dataProvider,
		));
	}        
        
	public function actionVer($id) {
		$this->render('ver', array(
			'model' => $this->loadModel($id, 'Alumno'),
		));
	}

	public function actionCrear() {
		$model = new Alumno;

		$this->performAjaxValidation($model, 'alumno-form');

		if (isset($_POST['Alumno'])) {
			$model->setAttributes($_POST['Alumno']);
			$relatedData = array(
				'cursos' => $_POST['Alumno']['cursos'] === '' ? null : $_POST['Alumno']['cursos'],
				);

			if ($model->saveWithRelated($relatedData)) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('crear', array( 'model' => $model));
	}

	public function actionActualizar($id) {
		$model = $this->loadModel($id, 'Alumno');

		$this->performAjaxValidation($model, 'alumno-form');

		if (isset($_POST['Alumno'])) {
			$model->setAttributes($_POST['Alumno']);
			$relatedData = array(
				'cursos' => $_POST['Alumno']['cursos'] === '' ? null : $_POST['Alumno']['cursos'],
				);

			if ($model->saveWithRelated($relatedData)) {
				$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('actualizar', array(
				'model' => $model,
				));
	}

	public function actionBorrar($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Alumno')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('administrar'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionAdministrar() {
                $session = new CHttpSession;
                $session->open();
		$model = new Alumno('search');
		$model->unsetAttributes();

		if (isset($_GET['Alumno'])){
			$model->setAttributes($_GET['Alumno']);
                }

                $session['Alumno_model_search'] = $model;
                
		$this->render('administrar', array(
			'model' => $model,
		));
	}
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
             
        
        public function actionGenerarExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Alumno_model_search']))
               {
                $model = $session['Alumno_model_search'];
                $model = Alumno::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Alumno::model()->findAll();
             $this->toExcel($model, array('id', 'nombre', 'run', 'direccion', 'user'), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGenerarPdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Alumno_model_search']))
               {
                $model = $session['Alumno_model_search'];
                $model = Alumno::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Alumno::model()->findAll();
             $this->toExcel($model, array('id', 'nombre', 'run', 'direccion', 'user'), date('Y-m-d-H-i-s'), array(), 'PDF');
	}

//-------------------------------------------------------------------------------------------------------------        
        
	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Alumno'),
		));
	}

	public function actionCreate() {
		$model = new Alumno;

		$this->performAjaxValidation($model, 'alumno-form');

		if (isset($_POST['Alumno'])) {
			$model->setAttributes($_POST['Alumno']);
			$relatedData = array(
				'cursos' => $_POST['Alumno']['cursos'] === '' ? null : $_POST['Alumno']['cursos'],
				);

			if ($model->saveWithRelated($relatedData)) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Alumno');

		$this->performAjaxValidation($model, 'alumno-form');

		if (isset($_POST['Alumno'])) {
			$model->setAttributes($_POST['Alumno']);
			$relatedData = array(
				'cursos' => $_POST['Alumno']['cursos'] === '' ? null : $_POST['Alumno']['cursos'],
				);

			if ($model->saveWithRelated($relatedData)) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Alumno')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

        //Index
	public function actionList() {
		$dataProvider = new CActiveDataProvider('Alumno');
                //$this->render('index', array(
		$this->render('list', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
                $session = new CHttpSession;
                $session->open();
		$model = new Alumno('search');
		$model->unsetAttributes();

		if (isset($_GET['Alumno'])){
			$model->setAttributes($_GET['Alumno']);
                }

                $session['Alumno_model_search'] = $model;
                
		$this->render('admin', array(
			'model' => $model,
		));
	}
}