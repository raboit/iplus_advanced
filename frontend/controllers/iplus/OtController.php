<?php

class OtController extends GxController {

        public function filters() {
                return array('rights');
        }
        public function actionJsonots()
        {

        }
	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Ot'),
		));
	}
        
        public function actionVer($id) {
            
            $model = $this->loadModel($id, 'Ot');
            if($model->validarDueno($model->cliente->user_id)){
		$this->render('ver', array(
			'model' => $model,
		));
            }
            
            else
               throw new CHttpException(400, Yii::t('app', 'You are not authorized to perform this action.'));
        
        }

	public function actionCreate() {
		$model = new Ot;

		$this->performAjaxValidation($model, 'ot-form');

		if (isset($_POST['Ot'])) {
			$model->setAttributes($_POST['Ot']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}
        
        public function actionCrear() {
		$model = new Ot;
                $model->fechaRegistro = date("Y/m/d H:i:s");
		$this->performAjaxValidation($model, 'ot-form');

		if (isset($_POST['Ot'])) {
			$model->setAttributes($_POST['Ot']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('ver', 'id' => $model->id));
			}
		}
                $busqueda_id = Yii::app()->user->getId();
                $model_clientes=Cliente::model()->findAll('user_id=:userid',array(':userid'=>$busqueda_id));
		$this->render('crear', array( 'model' => $model,'model_clientes'=>$model_clientes));
	}
        
        public function actionMisOts(){
            
            $model = new Ot;
            
            $filtersForm=new FiltersForm;
            if (isset($_GET['FiltersForm']))
                $filtersForm->filters=$_GET['FiltersForm'];

            // Get rawData and create dataProvider
            $model_ots=Ot::model()->with(array('cliente'=>array(
                'condition'=>'t.cliente_id=cliente.id AND cliente.user_id = :user_id',
                'params'=>array(':user_id'=>Yii::app()->user->getId()),
            )))->findAll();
            
            
            
            $dataProvider=new CArrayDataProvider($model_ots);
            $filteredData=$filtersForm->filter($dataProvider);
            
            $busqueda_id = Yii::app()->user->getId();
                $model_clientes=Cliente::model()->findAll('user_id=:userid',array(':userid'=>$busqueda_id));
		
            // Render
            $this->render('misOts', array(
                'filtersForm' => $filtersForm,
                'model' => $dataProvider,
                'model_ot' => $model,
                'model_clientes' => $model_clientes,
            ));         
            
            //$this->render('misOts',array('model_ots'=>$model_ots));
            
            
        }
        
        public function actionCrearCliente($cliente_id) {
                
		$model = new Ot;                
                $model_cliente = $this->loadModel($cliente_id, 'Cliente');
                
                if(!$model->validarDueno($model_cliente->user_id))
                   throw new CHttpException(400, Yii::t('app', 'You are not authorized to perform this action.'));
                
                $model->cliente_id = $cliente_id;
                $model->fechaRegistro = date("Y/m/d H:i:s");

		$this->performAjaxValidation($model, 'ot-form');

		if (isset($_POST['Ot'])) {
			$model->setAttributes($_POST['Ot']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('cliente/ver', 'id' => $model->cliente_id));
			}
		}

		$this->render('crearCliente', array( 'model' => $model, 'model_cliente' => $model_cliente));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Ot');

		$this->performAjaxValidation($model, 'ot-form');

		if (isset($_POST['Ot'])) {
			$model->setAttributes($_POST['Ot']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}
        
        public function actionActualizar($id) {
		$model = $this->loadModel($id, 'Ot');
                
                if($model->validarDueno($model->cliente->user_id)){
                    $this->performAjaxValidation($model, 'ot-form');

		if (isset($_POST['Ot'])) {
			$model->setAttributes($_POST['Ot']);

			if ($model->save()) {
				$this->redirect(array('ver', 'id' => $model->id));
			}
		}
                
                $busqueda_id = Yii::app()->user->getId();
                $model_clientes=Cliente::model()->findAll('user_id=:userid',array(':userid'=>$busqueda_id));
		$this->render('actualizar', array(
				'model' => $model,
                                'model_clientes' => $model_clientes,
				));
	}
               else
                    throw new CHttpException(400, Yii::t('app', 'You are not authorized to perform this action.'));
        
        }
        
        public function actionActualizarCliente($id) {
		$model = $this->loadModel($id, 'Ot');

		$this->performAjaxValidation($model, 'ot-form');

		if (isset($_POST['Ot'])) {
			$model->setAttributes($_POST['Ot']);

			if ($model->save()) {
				$this->redirect(array('ver', 'id' => $model->id));
			}
		}

		$this->render('actualizarCliente', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Ot')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}
        
        public function actionEliminar($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Ot')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('misOts'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Ot');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
                $session = new CHttpSession;
                $session->open();
		$model = new Ot('search');
		$model->unsetAttributes();

		if (isset($_GET['Ot'])){
			$model->setAttributes($_GET['Ot']);
                }

                $session['Ot_model_search'] = $model;
                
		$this->render('admin', array(
			'model' => $model,
		));
	}
        
        public function actionVerTodos() {
                $session = new CHttpSession;
                $session->open();
		$model = new Ot('search');
		$model->unsetAttributes();

		if (isset($_GET['Ot'])){
			$model->setAttributes($_GET['Ot']);
                }

                $session['Ot_model_search'] = $model;
                
		$this->render('verTodos', array(
			'model' => $model,
		));
	}
        
        public function behaviors()
        {
            return array(
                'eexcelview'=>array(
                    'class'=>'ext.eexcelview.EExcelBehavior',
                ),
            );
        }
        
        public function actionGenerateExcel()
	{	   
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Ot_model_search']))
               {
                $model = $session['Ot_model_search'];
                $model = Ot::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Ot::model()->findAll();
             
             $this->toExcel($model, array('id', 'cliente', 'fechaRegistro', 'caracteristicas', 'problema', 'solucion', 'precio', 'observaciones', 'estadoOt', 'fechaTermino', 'fechaEntrega', 'estadoCuenta'), date('Y-m-d-H-i-s'), array(), 'Excel5');
	}
        
        public function actionGeneratePdf() 
	{
             $session=new CHttpSession;
             $session->open();
             if(isset($session['Ot_model_search']))
               {
                $model = $session['Ot_model_search'];
                $model = Ot::model()->findAll($model->search()->criteria);
               }
               else
                 $model = Ot::model()->findAll();
             
             $this->toExcel($model, array('id', 'cliente', 'fechaRegistro', 'caracteristicas', 'problema', 'solucion', 'precio', 'observaciones', 'estadoOt', 'fechaTermino', 'fechaEntrega', 'estadoCuenta'), date('Y-m-d-H-i-s'), array(), 'PDF');
	}
        
        public function actionFinalizarOt($id){
            
            $model = $this->loadModel($id, 'Ot');
            
            $user_id = $model->cliente->user_id;
            if(!$model->validarDueno($user_id))
                   throw new CHttpException(400, Yii::t('app', 'You are not authorized to perform this action.'));
                            
            $model->fechaTermino = date("Y/m/d H:i:s");
            $model->estadoOt = "FINALIZADO";
            $model->save(FALSE);
            $this->redirect(array('ver', 'id' => $id));
        }
        
        public function actionEntregarOt($id){
            
            $model = $this->loadModel($id, 'Ot');
            
            $user_id = $model->cliente->user_id;
            if(!$model->validarDueno($user_id))
                   throw new CHttpException(400, Yii::t('app', 'You are not authorized to perform this action.'));
                
            $model->fechaEntrega = date("Y/m/d H:i:s");
            $model->estadoOt = "ENTREGADO";
            $model->save(FALSE);
            $this->redirect(array('ver', 'id' => $id));
        }
        
        public function actionPagarOt($id){
            
            $model = $this->loadModel($id, 'Ot');
            
            $user_id = $model->cliente->user_id;
            if(!$model->validarDueno($user_id))
                   throw new CHttpException(400, Yii::t('app', 'You are not authorized to perform this action.'));
                
            
            $model->estadoCuenta = "PAGADO";
            $model->save(FALSE);
            $this->redirect(array('ver', 'id' => $id));
        }
        
}
