<div class="form">

    
<?php $this->widget('ext.EChosen.EChosen', array('target' => 'select')); ?>
<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'ot-form',
	'enableAjaxValidation' => true,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'cliente_id'); ?>
		<?php echo CHtml::encode($model_cliente->nombre); ?>
		<?php echo $form->error($model,'cliente_id'); ?>
		</div><!-- row -->
                <div class="row">
		<?php echo $form->labelEx($model,'caracteristicas'); ?>
		<?php echo $form->textField($model, 'caracteristicas', array('maxlength' => 200)); ?>
		<?php echo $form->error($model,'caracteristicas'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'problema'); ?>
		<?php echo $form->textField($model, 'problema', array('maxlength' => 200)); ?>
		<?php echo $form->error($model,'problema'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'solucion'); ?>
		<?php echo $form->textField($model, 'solucion', array('maxlength' => 200)); ?>
		<?php echo $form->error($model,'solucion'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'precio'); ?>
		<?php echo $form->textField($model, 'precio'); ?>
		<?php echo $form->error($model,'precio'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'observaciones'); ?>
		<?php echo $form->textField($model, 'observaciones', array('maxlength' => 200)); ?>
		<?php echo $form->error($model,'observaciones'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'estadoCuenta'); ?>
		<?php echo $form->dropDownList($model, 'estadoCuenta', array('PENDIENTE' => 'Pendiente', 'PAGADO' => 'Pagado')); ?>
		<?php echo $form->error($model,'estadoCuenta'); ?>
		</div><!-- row -->


<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'), array('onClick' => "this.disabled=true;this.value='".Yii::t('app', 'Enviando')."';this.form.submit();"));
$this->endWidget();
?>
</div><!-- form -->