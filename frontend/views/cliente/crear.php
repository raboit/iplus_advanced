<?php

$this->breadcrumbs = array(
	$model->label(2) => array('listar'),
	Yii::t('app', 'Create'),
);

$this->menu = array(
        array('label'=>Yii::t('app', 'Operations')),
        array('label'=>Yii::t('app', 'Mis') . ' ' . $model->label(2), 'url' => array('cliente/misClientes'),  'icon'=>'briefcase white'),
        array('label'=>Yii::t('app', 'Other|Others', 2)),
        array('label'=>Yii::t('app', 'Back'), 'url'=>'javascript:history.back()', 'icon'=>'arrow-left white'),
    array('label'=>Yii::t('app', 'Up'), 'url'=>'javascript:GoUp()', 'icon'=>'arrow-up white', 'id'=>'button-up'),
);
?>

<?php echo TbHtml::pageHeader(Yii::t('app', 'Create') . ' ' . GxHtml::encode($model->label()), null); ?>
<?php
$this->renderPartial('_formulario', array(
		'model' => $model,
		'buttons' => 'create'));
?>