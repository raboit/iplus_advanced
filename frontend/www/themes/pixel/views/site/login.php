<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="">
        <div class="logo animate0 bounceIn">
            <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/logo_sisega.png" alt="" />
        </div>
        
        <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>
        
            <div class="inputwrapper login-alert">
                <div class="alert alert-error">Invalid username or password</div>
            </div>
        
            <div class="inputwrapper animate1 bounceIn">
		<?php echo $form->textField($model,'username'); ?>
		<?php echo $form->error($model,'username'); ?>
            </div>
            <div class="inputwrapper animate2 bounceIn">
                <?php echo $form->passwordField($model,'password'); ?>
                <?php echo $form->error($model,'password'); ?>
            </div>

            <div class="inputwrapper animate3 bounceIn">
                <?php echo $form->checkBox($model,'rememberMe'); ?>
                <?php echo $form->error($model,'rememberMe'); ?>
            </div>

            <div class="inputwrapper animate4 bounceIn">
                <?php echo CHtml::submitButton(Yii::t('app', 'Login')); ?>
            </div>
            
        
    <?php $this->endWidget(); ?>
    </div>
    