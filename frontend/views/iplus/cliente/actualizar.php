<?php

$this->breadcrumbs = array(
	"Mis Cliente" => array('misClientes'),
        "Ver Cliente" => array('ver', 'id' => $model->id),
	Yii::t('app', 'Update'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Back'), 'url'=>Yii::app()->request->urlReferrer),
);
?>

<h1><?php echo Yii::t('app', 'Update') . ' ' . GxHtml::encode($model->label()) . ': ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php
$this->renderPartial('_crear', array(
		'model' => $model));
?>