<?php $this->widget('ext.EChosen.EChosen' ); ?><div class="wide form">

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'search-ot-form',
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'id'); ?>
		<?php echo $form->textField($model, 'id'); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'cliente_id'); ?>
		<?php echo $form->dropDownList($model, 'cliente_id', GxHtml::listDataEx(Cliente::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'fechaRegistro'); ?>
		<?php echo $form->textField($model, 'fechaRegistro'); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'caracteristicas'); ?>
		<?php echo $form->textField($model, 'caracteristicas', array('maxlength' => 200)); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'problema'); ?>
		<?php echo $form->textField($model, 'problema', array('maxlength' => 200)); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'solucion'); ?>
		<?php echo $form->textField($model, 'solucion', array('maxlength' => 200)); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'precio'); ?>
		<?php echo $form->textField($model, 'precio'); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'observaciones'); ?>
		<?php echo $form->textField($model, 'observaciones', array('maxlength' => 200)); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'estadoOt'); ?>
		<?php echo $form->textField($model, 'estadoOt', array('maxlength' => 45)); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'fechaTermino'); ?>
		<?php echo $form->textField($model, 'fechaTermino'); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'fechaEntrega'); ?>
		<?php echo $form->textField($model, 'fechaEntrega'); ?>
	</div>
                
	<div class="row">
		<?php echo $form->label($model, 'estadoCuenta'); ?>
		<?php echo $form->textField($model, 'estadoCuenta', array('maxlength' => 45)); ?>
	</div>
                
	<div class="row buttons">
		<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'label'=>Yii::t('app', 'Search'), 'icon'=>'search'));?>
		<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'button', 'label'=>Yii::t('app', 'Reset'), 'icon'=>'icon-remove-sign', 'htmlOptions'=>array('class'=>'btnreset btn-small')));?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
<script>
     $(".btnreset").click(function(){
             $(":input","#search-ot-form").each(function() {
             var type = this.type;
             var tag = this.tagName.toLowerCase(); // normalize case
             if (type == "text" || type == "password" || tag == "textarea") this.value = "";
             else if (type == "checkbox" || type == "radio") this.checked = false;
             else if (tag == "select") this.selectedIndex = "";
       });
     });
</script>

