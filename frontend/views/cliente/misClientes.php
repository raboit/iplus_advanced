<?php
$this->breadcrumbs = array(
	$model->label(2) => array('listar'),
	Yii::t('app', 'My clients'),
);

$this->menu = array(
        array('label'=>Yii::t('app', 'Operations')),
        array('label'=>Yii::t('app', 'Create') . ' Cliente', 'url'=>array('crear'), 'icon'=>'file white'),
        array('label'=>Yii::t('app', 'Export')),
        array('label'=>Yii::t('app', 'Export to Excel'), 'url'=>Yii::app()->controller->createUrl('GenerarExcel'), 'linkOptions'=>array('target'=>'_blank'), 'icon'=>'download-alt white'),
        array('label'=>Yii::t('app', 'Other|Others', 2)),
        array('label'=>Yii::t('app', 'Back'), 'url'=>'javascript:history.back()', 'icon'=>'arrow-left white'),
    array('label'=>Yii::t('app', 'Up'), 'url'=>'javascript:GoUp()', 'icon'=>'arrow-up white', 'id'=>'button-up'),
);
?>
<?php echo TbHtml::pageHeader(Yii::t('app', 'View') . '  Clientes ' . GxHtml::encode(GxHtml::valueEx($model)), null); ?>
<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
            'username',
            'email',
	),
)); ?>

<p></br>
<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered',
    'dataProvider' => $model_clientes,
    'filter'=>new cliente,
    'enableSorting'=>false,
    'template'=> '{summary}{pager}{items}{pager}',
    'columns' => array(
                'nombre',
                'telefono',
                'celular',
                'email',
                
                array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        'htmlOptions'=>array('style'=>'width: 50px'),
			'template'=>'{view} {update}',
			'buttons'=>array(
				'view' => array(
					'label'=>'Ver Cliente',
					'url'=>'Yii::app()->createUrl("cliente/ver", array("id"=>$data->id))',
				),
				'update' => array(
					'label'=>'Actualizar Cliente',
					'url'=>'Yii::app()->createUrl("cliente/actualizar", array("id"=>$data->id))',
				),
			),
				
                ),
    ),
));?>
