<?php

$this->breadcrumbs = array(
	$model->label(2) => array('listar'),
	GxHtml::valueEx($model),
);

$this->menu=array(
        array('label'=>Yii::t('app', 'Operations'),),
        array('label'=>Yii::t('app', 'Create') . ' OT', 'url'=>Yii::app()->createUrl('ot/crearCliente',array('id'=>$model->id)), 'icon'=>'file white'),
        array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('actualizar', 'id' => $model->id), 'icon'=>'pencil white'),
        array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('borrar', 'id' => $model->id), 'confirm'=>Yii::t('app', 'Are you sure you want to delete this item?')), 'icon'=>'trash white'),

        array('label'=>Yii::t('app', 'Other|Others', 2)),
        array('label'=>Yii::t('app', 'Back'), 'url'=>'javascript:history.back()', 'icon'=>'arrow-left white'),
    array('label'=>Yii::t('app', 'Up'), 'url'=>'javascript:GoUp()', 'icon'=>'arrow-up white', 'id'=>'button-up'),
);
?>

<?php echo TbHtml::pageHeader(Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)), null); ?>


<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
'telefono',
'celular',
'email',
	),
)); ?>

<?php
$this->widget('bootstrap.widgets.TbGridView', array(
    'type'=>'striped bordered',
    'dataProvider' => $model_ots,
    'enableSorting'=>false,
    'template'=> '{summary}{pager}{items}{pager}',
    'columns' => array(
                'fechaRegistro',
                'caracteristicas',
        	'problema',
                'solucion',
                'precio',
                'estadoOt',
                /*'fechaTermino',
                'fechaEntrega',*/
                'estadoCuenta',
                array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
                        'htmlOptions'=>array('style'=>'width: 50px'),
			'template'=>'{view} {update}',
			'buttons'=>array(
				'view' => array(
					'label'=>'Ver Ot',
					'url'=>'Yii::app()->createUrl("ot/ver", array("id"=>$data->id))',
				),
				'update' => array(
					'label'=>'Actualizar Ot',
					'url'=>'Yii::app()->createUrl("ot/actualizar", array("id"=>$data->id))',
				),
			),
				
                ),
    ),
));?>