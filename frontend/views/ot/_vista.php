<a class="buttonwell" href="<?php echo Yii::app()->controller->createUrl('ver', array('id' => $data->id));?>">
<div class="view well">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
        <?php echo GxHtml::encode($data->id); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('cliente_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->cliente)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('fechaRegistro')); ?>:
        <?php echo GxHtml::encode($data->fechaRegistro); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('caracteristicas')); ?>:
        <?php echo GxHtml::encode($data->caracteristicas); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('problema')); ?>:
        <?php echo GxHtml::encode($data->problema); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('solucion')); ?>:
        <?php echo GxHtml::encode($data->solucion); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('precio')); ?>:
        <?php echo GxHtml::encode($data->precio); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('observaciones')); ?>:
        <?php echo GxHtml::encode($data->observaciones); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('estadoOt')); ?>:
        <?php echo GxHtml::encode($data->estadoOt); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('fechaTermino')); ?>:
        <?php echo GxHtml::encode($data->fechaTermino); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('fechaEntrega')); ?>:
        <?php echo GxHtml::encode($data->fechaEntrega); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('estadoCuenta')); ?>:
        <?php echo GxHtml::encode($data->estadoCuenta); ?>
	<br />
	*/ ?>

</div>
</a>